describe('delete user', () => {
    beforeEach(() => {
        cy.server();
    });

    it('should delete user from users list page', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {
                data: [
                    {_id: '1', firstName: 'Bruce', lastName: 'Lee', email: 'bruce@lee.com'},
                    {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
                    {_id: '3', firstName: 'User 3 First Name', lastName: 'User 3 Last Name', email: 'user3@mail.com'},
                ],
            },
        }).as('getUsers');

        cy.route({
            method: 'DELETE',
            url: '**/api/users/1',
            response: {
                data: [
                    {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
                    {_id: '3', firstName: 'User 3 First Name', lastName: 'User 3 Last Name', email: 'user3@mail.com'},
                ],
            },
        }).as('deleteUser');
        cy.visit('http://localhost:3000');

        cy.get('[data-testid=table-body-data]')
            .contains('tr', 'bruce@lee.com')
            .find('[data-testid=delete-button]')
            .click();
        cy.get('[data-testid=confirm-delete-button]').click();
        cy.wait('@deleteUser');
        cy.get('[data-testid=table-body-data] > tr').should('not.contain', 'bruce@lee.com');
    });

    it('should delete user from user page', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {
                data: [
                    {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
                    {_id: '3', firstName: 'User 3 First Name', lastName: 'User 3 Last Name', email: 'user3@mail.com'},
                ],
            },
        }).as('getUsers');

        cy.route({
            method: 'DELETE',
            url: '**/api/users/1',
            response: {
                data: [
                    {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
                    {_id: '3', firstName: 'User 3 First Name', lastName: 'User 3 Last Name', email: 'user3@mail.com'},
                ],
            },
        }).as('deleteUser');
        cy.route({
            method: 'GET',
            url: '**/api/users/1',
            response: {
                data: {_id: '1', firstName: 'Bruce', lastName: 'Lee', email: 'bruce@lee.com'},
            },
        }).as('getUser');

        cy.visit('http://localhost:3000/users/1');
        cy.wait('@getUser');

        cy.get('[data-testid=user-delete-button]').click();
        cy.wait('@deleteUser');
        cy.location('pathname').should('eq', '/users');
        cy.get('[data-testid=table-body-data] > tr').should('not.contain', 'bruce@lee.com');
    });
});
