describe('edit user', () => {
    beforeEach(() => {
        cy.server();
    });

    it('should redirect to the edit page from users list page', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {
                data: [
                    {_id: '1', firstName: 'User 1 First Name', lastName: 'User 1 Last Name', email: 'user1@mail.com'},
                    {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
                ],
            },
        }).as('getUsers');

        cy.route({
            method: 'GET',
            url: '**/api/users/*',
            response: {
                data: {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
            },
        }).as('getUser');
        cy.visit('http://localhost:3000');
        cy.wait('@getUsers');
        cy.get('[data-testid=table-body-data]')
            .contains('tr', 'User 2 First Name')
            .find('[data-testid=edit-button]')
            .click();

        cy.location('pathname').should('eq', '/users/2');
        cy.get('form').within(() => {
            cy.get('input[aria-label=input-firstName]').should('have.value', 'User 2 First Name');
            cy.get('input[aria-label=input-lastName]').should('have.value', 'User 2 Last Name');
            cy.get('input[aria-label=input-email]').should('have.value', 'user2@mail.com');
        });
    });

    it('should edit user with valid data', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {
                data: [
                    {_id: '1', firstName: 'User 1 First Name', lastName: 'User 1 Last Name', email: 'user1@mail.com'},
                    {
                        _id: '2',
                        firstName: 'User 2 First Name Changed',
                        lastName: 'User 2 Last Name Changed',
                        email: 'user2@mail.com',
                    },
                ],
            },
        }).as('getUsers');

        cy.route({
            method: 'GET',
            url: '**/api/users/*',
            response: {
                data: {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
            },
        }).as('getUser');
        cy.route({
            method: 'PUT',
            url: '**/api/users/2',
            response: [],
        }).as('updateUser');

        cy.visit('http://localhost:3000/users/2');
        cy.wait('@getUser');
        cy.get('form').within(() => {
            cy.get('input[aria-label=input-firstName]').type(' Changed');
            cy.get('input[aria-label=input-lastName]').type(' Changed');
        });
        cy.get('form')
            .should('have.not.contain.text', 'Required')
            .should('have.not.contain.text', 'Invalid');

        cy.get('[data-testid=save-button]').should('not.be.disabled');

        cy.get('[data-testid=save-button]').click();
        cy.wait('@updateUser');
        cy.location('pathname').should('eq', '/users');
        cy.get('[data-testid=table-body-data]').contains('tr', 'User 2 Last Name Changed');
    });

    it('should not update user with invalid data', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users/*',
            response: {
                data: {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
            },
        }).as('getUser');

        cy.visit('http://localhost:3000/users/2');
        cy.wait('@getUser');

        cy.get('form').within(() => {
            cy.get('input[aria-label=input-firstName]').clear();
            cy.get('input[aria-label=input-email]').type('fooo@dd');
        });
        cy.get('[data-testid=save-button]').should('be.disabled');
        cy.get('form')
            .should('have.contain.text', 'Required')
            .should('have.contain.text', 'Invalid');
    });
});
