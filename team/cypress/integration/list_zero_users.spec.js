describe('listing users', () => {
    beforeEach(() => {
        cy.server();
    });

    it('should list zero users', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {data: []},
        }).as('getUsers');
        cy.visit('http://localhost:3000');
        cy.wait('@getUsers');
        cy.get('[data-testid=table-body-data] > tr > td').should('contain', 'No users found!');
    });

    it('should list some users', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {
                data: [
                    {_id: '1', firstName: 'User 1 First Name', lastName: 'User 1 Last Name', email: 'user1@mail.com'},
                    {_id: '2', firstName: 'User 2 First Name', lastName: 'User 2 Last Name', email: 'user2@mail.com'},
                ],
            },
        }).as('getSomeUsers');
        cy.visit('http://localhost:3000');
        cy.wait('@getSomeUsers');
        cy.get('[data-testid=table-body-data]')
            .find('tr')
            .should('have.length', 2);

        cy.get('[data-testid=table-body-data] > :nth-child(1) > :nth-child(2)').should(
            'have.text',
            'User 1 First Name',
        );
    });

    it('should edit user of the list', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {
                data: [
                    {_id: '1', firstName: 'User 1 First Name', lastName: 'User 1 Last Name', email: 'user1@mail.com'},
                    {_id: '2', firstName: 'Bruce', lastName: 'Lee', email: 'bruce@lee.com'},
                ],
            },
        }).as('getSomeUsers');

        cy.route({
            method: 'GET',
            url: '**/api/users/2',
            response: {
                data: {_id: '2', firstName: 'Bruce', lastName: 'Lee', email: 'bruce@lee.com'},
            },
        }).as('getUser');

        cy.visit('http://localhost:3000');
        cy.wait('@getSomeUsers');

        cy.get('[data-testid=table-body-data]')
            .contains('tr', 'Bruce')
            .find('[data-testid=edit-button]')
            .click();

        cy.location('pathname').should('eq', '/users/2');

        cy.wait('@getUser');

        cy.get('form').within(() => {
            cy.get('input:first').should('have.value', 'Bruce');
        });
    });
});
