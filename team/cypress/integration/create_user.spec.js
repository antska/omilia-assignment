describe('creating user', () => {
    beforeEach(() => {
        cy.server();
    });

    it('should redirect to the create page with empty form', () => {
        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {data: []},
        }).as('getUsers');
        cy.visit('http://localhost:3000');

        cy.wait('@getUsers');
        cy.get('[data-testid=create-button]').click();
        cy.location('pathname').should('eq', '/users/create');
        cy.get('form').within(() => {
            cy.get('input').should('have.value', '');
        });
    });

    it('should create user with valid data', () => {
        cy.visit('http://localhost:3000/users/create');

        cy.route({
            method: 'GET',
            url: '**/api/users',
            response: {data: [{_id: '1', firstName: 'Bruce', lastName: 'Lee', email: 'bruce@lee.com'}]},
        }).as('getUsers');
        cy.route({
            method: 'POST',
            url: '**/api/users',
            response: [],
        }).as('createUser');

        cy.get('form').within(() => {
            cy.get('input[aria-label=input-firstName]').type('Bruce');
            cy.get('input[aria-label=input-lastName]').type('Lee');
            cy.get('input[aria-label=input-email]').type('bruce@lee.com');
        });
        cy.get('form')
            .should('have.not.contain.text', 'Required')
            .should('have.not.contain.text', 'Invalid');

        cy.get('[data-testid=save-button]').should('not.be.disabled');

        cy.get('[data-testid=save-button]').click();
        cy.wait('@createUser');
        cy.location('pathname').should('eq', '/users');
        cy.get('[data-testid=table-body-data]').contains('tr', 'Bruce');
    });

    it('should not create user with invalid data', () => {
        cy.visit('http://localhost:3000/users/create');

        cy.get('form').within(() => {
            cy.get('input[aria-label=input-firstName]').type('Bruce');
            cy.get('input[aria-label=input-lastName]')
                .type('Lee')
                .clear();
            cy.get('input[aria-label=input-email]').type('brucelee.com');
        });
        cy.get('[data-testid=save-button]').should('be.disabled');
        cy.get('form')
            .should('have.contain.text', 'Required')
            .should('have.contain.text', 'Invalid');
    });
});
