import React from 'react';
import {Switch, Route, Redirect, useLocation} from 'react-router-dom';
import Navbar from './components/Navbar';
import UsersListPage from './pages/UsersListPage';
import UserPage from './pages/UserPage';
import ErrorPage from './pages/ErrorPage';

const App = () => {
    const location = useLocation();
    return (
        <div>
            <Navbar location={location} />
            <Switch>
                <Route exact path="/users" component={UsersListPage} />
                <Route exact path="/">
                    <Redirect to="/users" />
                </Route>
                <Route exact path="/users/create" component={UserPage} />
                <Route exact path="/users/:id" component={UserPage} />
                <Route exact path="/error" component={ErrorPage} />
                <Route component={ErrorPage} />
            </Switch>
        </div>
    );
};
export default App;
