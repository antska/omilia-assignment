import React from 'react';
import {render} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';
import UsersListPage from '../pages/UsersListPage';
import {loadingSelector, errorSelector} from '../store/selectors';
import * as actions from '../store/actions';

jest.spyOn(actions, 'getUsersList');

afterEach(() => {
    loadingSelector.mockRestore();
    errorSelector.mockRestore();
    jest.clearAllMocks();
});

describe('testing Users List Page', () => {
    it('should render correctly', () => {
        const {getByTestId} = render(
            <BrowserRouter>
                <UsersListPage />
            </BrowserRouter>,
        );
        expect(getByTestId('home-container')).toBeInTheDocument();
    });

    it('should call dispatch getUsersList once', () => {
        render(
            <BrowserRouter>
                <UsersListPage />
            </BrowserRouter>,
        );
        expect(actions.getUsersList).toHaveBeenCalledTimes(1);
    });

    it('should show loading', () => {
        loadingSelector.mockReturnValue(true);
        const {getByTestId} = render(
            <BrowserRouter>
                <UsersListPage />
            </BrowserRouter>,
        );

        const loadingBar = getByTestId('loading-bar');
        expect(loadingBar).toBeInTheDocument();
    });

    it('should redirect to error page if error found', () => {
        errorSelector.mockReturnValue(true);
        const {queryByTestId} = render(
            <BrowserRouter>
                <UsersListPage />
            </BrowserRouter>,
        );

        expect(window.location.pathname).toBe('/error');
        expect(queryByTestId('home-container')).not.toBeInTheDocument();
    });
});
