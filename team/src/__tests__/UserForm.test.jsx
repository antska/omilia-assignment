/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {render, waitForElement, fireEvent} from '@testing-library/react';
import {loadingSelector, errorSelector, userDataSelector} from '../store/selectors';
import UserForm from '../components/UserForm';
import FIELDS from '../fields';
import * as actions from '../store/actions';

jest.spyOn(actions, 'deleteUser');
jest.spyOn(actions, 'updateUser');

afterEach(() => {
    loadingSelector.mockRestore();
    errorSelector.mockRestore();
    userDataSelector.mockRestore();
    jest.clearAllMocks();
});

describe('testing User Form', () => {
    it('should render correctly on create', () => {
        const props = {
            user: {},
            fields: {},
            action: 'create',
        };
        const {getByText} = render(<UserForm {...props} />);
        expect(getByText('CREATE USER')).toBeInTheDocument();
    });

    it('should render correctly on edit', () => {
        const props = {
            user: {},
            fields: {},
            action: 'edit',
        };
        const {getByText} = render(<UserForm {...props} />);
        expect(getByText('EDIT USER')).toBeInTheDocument();
    });

    it('should show fields in form', () => {
        const props = {
            user: {},
            fields: FIELDS,
            action: 'create',
        };
        const {queryByText} = render(<UserForm {...props} />);
        expect(queryByText(/First Name/i)).toBeInTheDocument();
    });

    it('should show user details in form (edit)', () => {
        const props = {
            user: {id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'},
            fields: FIELDS,
            action: 'edit',
        };
        const {getByText} = render(<UserForm {...props} />);
        waitForElement(() => expect(getByText('testName')).toBeInTheDocument());
    });

    it('should have disabled save button on empty form', () => {
        const props = {
            user: {},
            fields: FIELDS,
            action: 'create',
        };
        const {getByTestId} = render(<UserForm {...props} />);
        expect(getByTestId('save-button')).toBeDisabled();
    });

    it('should have disabled save button on NOT completed form', () => {
        const props = {
            user: {},
            fields: FIELDS,
            action: 'create',
        };
        const {getByTestId, getByLabelText} = render(<UserForm {...props} />);

        const firstNameValue = getByLabelText('input-firstName');

        fireEvent.change(firstNameValue, {target: {value: 'test first'}});

        expect(getByTestId('save-button')).toBeDisabled();
    });

    it('should have disabled save button on wrong email', () => {
        const props = {
            user: {},
            fields: FIELDS,
            action: 'create',
        };
        const {queryByText, getByTestId, getByLabelText} = render(<UserForm {...props} />);

        const firstNameValue = getByLabelText('input-firstName');
        const lastNameValue = getByLabelText('input-lastName');
        const emailValue = getByLabelText('input-email');

        fireEvent.change(firstNameValue, {target: {value: 'test first'}});
        fireEvent.change(lastNameValue, {target: {value: 'test last'}});
        fireEvent.change(emailValue, {target: {value: 'email.com'}});

        expect(queryByText(/Invalid email address/i)).toBeInTheDocument();
        expect(getByTestId('save-button')).toBeDisabled();
    });

    it('should NOT have disabled save button on correct form', () => {
        const props = {
            user: {},
            fields: FIELDS,
            action: 'create',
        };
        const {getByLabelText, getByTestId} = render(<UserForm {...props} />);

        const firstNameValue = getByLabelText('input-firstName');
        const lastNameValue = getByLabelText('input-lastName');
        const emailValue = getByLabelText('input-email');

        fireEvent.change(firstNameValue, {target: {value: 'test first'}});
        fireEvent.change(lastNameValue, {target: {value: 'test last'}});
        fireEvent.change(emailValue, {target: {value: 'email@email.com'}});

        expect(getByTestId('save-button')).not.toHaveAttribute('disabled');
    });

    it('should NOT have disabled save button on user loaded', () => {
        const props = {
            user: {id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'},
            fields: FIELDS,
            action: 'edit',
        };
        const {getByTestId} = render(<UserForm {...props} />);

        waitForElement(() => expect(getByTestId('save-button')).toBeDisabled(false));
    });

    it('should dispatch once on submit and redirect home', () => {
        const props = {
            user: {id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'},
            fields: FIELDS,
            action: 'edit',
        };
        const {getByTestId} = render(<UserForm {...props} />);
        const saveBtn = getByTestId('save-button');
        fireEvent.click(saveBtn);
        expect(actions.updateUser).toHaveBeenCalledTimes(1);
        expect(window.location.pathname).toBe('/');
    });

    it('should dispatch once on delete and redirect home', () => {
        const props = {
            user: {id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'},
            fields: FIELDS,
            action: 'edit',
        };
        const {getByTestId} = render(<UserForm {...props} />);
        const deleteBtn = getByTestId('user-delete-button');
        fireEvent.click(deleteBtn);
        expect(actions.deleteUser).toHaveBeenCalledTimes(1);
        expect(window.location.pathname).toBe('/');
    });
});
