import configureMockStore from 'redux-mock-store'; // mock store
import thunk from 'redux-thunk';

import {getUsersList, getUser, createUser, updateUser, deleteUser} from '../store/actions';
import {
    GET_USER_SUCCESS,
    REQUEST_STARTED,
    GET_LIST_SUCCESS,
    DELETE_USER_SUCCESS,
    REQUEST_FAILED,
    CREATE_USER_SUCCESS,
    UPDATE_USER_SUCCESS,
} from '../store/constants';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('testing API with dispatched actions', () => {
    beforeEach(() => {
        fetch.resetMocks();
    });
    it('dispatches getUsersList and gets correct data and actions', async () => {
        const payload = [{id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'}];
        fetch.mockResponseOnce(JSON.stringify({data: payload}));

        const expectedActions = [{type: REQUEST_STARTED}, {type: GET_LIST_SUCCESS, listData: payload}];

        const store = mockStore({listData: []});
        await store.dispatch(getUsersList());
        expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches getUser and gets correct data and actions', async () => {
        const payload = {id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'};
        fetch.mockResponseOnce(JSON.stringify({data: payload}));

        const expectedActions = [{type: REQUEST_STARTED}, {type: GET_USER_SUCCESS, userData: payload}];

        const store = mockStore({userData: []});
        await store.dispatch(getUser());
        expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches createUser and gets correct data and actions', async () => {
        const payload = [
            {id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'},
            {id: '2345', firstName: 'test2Name', lastName: 'test2Last', email: 'test2@last.com'},
        ];
        fetch.mockResponseOnce(JSON.stringify({data: payload}));

        const expectedActions = [{type: REQUEST_STARTED}, {type: CREATE_USER_SUCCESS, listData: payload}];

        const store = mockStore({
            listData: [{id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'}],
        });
        await store.dispatch(
            createUser({id: '2345', firstName: 'test2Name', lastName: 'test2Last', email: 'test2@last.com'}),
        );
        expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches updateUser and gets correct data and actions', async () => {
        const data = {id: '1234', firstName: 'correctName', lastName: 'testLast', email: 'test@last.com'};
        fetch.mockResponseOnce(JSON.stringify({data: [data]}));

        const expectedActions = [{type: REQUEST_STARTED}, {type: UPDATE_USER_SUCCESS, listData: [data]}];
        const store = mockStore({
            listData: [{id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'}],
        });
        await store.dispatch(updateUser('1234', data));
        expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches deleteUser and gets correct data and actions', async () => {
        const data = {};
        fetch.mockResponseOnce(JSON.stringify({data: [data]}));

        const expectedActions = [{type: REQUEST_STARTED}, {type: DELETE_USER_SUCCESS, listData: [data], goHome: true}];
        const store = mockStore({
            listData: [{id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'}],
        });
        await store.dispatch(deleteUser('1234', true));
        expect(store.getActions()).toEqual(expectedActions);
    });

    it('dispatches action but gets failed', async () => {
        fetch.mockReject(new Error('fake error message'));

        const expectedActions = [{type: REQUEST_STARTED}, {type: REQUEST_FAILED, error: 'fake error message'}];
        const store = mockStore({error: null});
        await store.dispatch(getUsersList());
        expect(store.getActions()).toEqual(expectedActions);
    });
});
