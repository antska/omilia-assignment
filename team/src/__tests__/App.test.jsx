import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import {render, waitForElement} from '@testing-library/react';
import App from '../App';

describe('testing App', () => {
    it('should redirect to /users', () => {
        const {getByTestId} = render(
            <MemoryRouter initialEntries={['/']}>
                <App />
            </MemoryRouter>,
        );
        waitForElement(() => expect(window.location.pathname).toBe('/users'));
        expect(getByTestId('home-container')).toBeInTheDocument();
    });
    it('should redirect to /error', () => {
        const {getByTestId, queryByTestId} = render(
            <MemoryRouter initialEntries={['/foo']}>
                <App />
            </MemoryRouter>,
        );
        waitForElement(() => expect(window.location.pathname).toBe('/error'));
        expect(getByTestId('error-message')).toBeInTheDocument();
        expect(queryByTestId('home-container')).not.toBeInTheDocument();
    });
});
