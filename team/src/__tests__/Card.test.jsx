import React from 'react';
import {render} from '@testing-library/react';
import Card from '../components/Card';

describe('testing Card component', () => {
    it('should render correctly with title and children props', () => {
        const {container} = render(
            <Card title="Test Title">
                <div>Test Component</div>
            </Card>,
        );
        expect(container.textContent).toContain('Test Title');
        expect(container.textContent).toContain('Test Component');
    });

    it('should render correctly without title prop', () => {
        const {container, queryByTestId} = render(
            <Card>
                <div>Test Component</div>
            </Card>,
        );
        expect(queryByTestId('card-menu-title')).not.toBeInTheDocument();
        expect(container.textContent).toContain('Test Component');
    });
});
