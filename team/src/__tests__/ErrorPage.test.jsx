import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import {BrowserRouter, MemoryRouter} from 'react-router-dom';
import ErrorPage from '../pages/ErrorPage';
import {errorSelector} from '../store/selectors';

describe('testing Error Page', () => {
    it('should render correctly', () => {
        const {getByTestId} = render(
            <BrowserRouter>
                <ErrorPage />
            </BrowserRouter>,
        );
        expect(getByTestId('error-message')).toBeInTheDocument();
    });

    it('should show error on page', () => {
        errorSelector.mockReturnValue('500 internal error');

        const {getByText} = render(
            <BrowserRouter>
                <ErrorPage />
            </BrowserRouter>,
        );
        expect(getByText('500 internal error')).toBeInTheDocument();
    });

    it('clicks on back button redirect to home', () => {
        const {getByTestId} = render(
            <MemoryRouter initialEntries={['/error/']}>
                <ErrorPage />
            </MemoryRouter>,
        );

        const backBtn = getByTestId('error-home-button');
        fireEvent.click(backBtn);
        expect(window.location.pathname).toBe('/');
    });
});
