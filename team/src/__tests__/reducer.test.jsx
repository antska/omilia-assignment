import {reducer} from '../store/reducers';
import {
    REQUEST_STARTED,
    GET_USER_SUCCESS,
    GET_LIST_SUCCESS,
    DELETE_USER_SUCCESS,
    CREATE_USER_SUCCESS,
    UPDATE_USER_SUCCESS,
    REQUEST_FAILED,
    RESET,
    RESET_USER,
} from '../store/constants';

describe('testing reducer', () => {
    const initialState = {
        loading: false,
        listData: null,
        error: null,
        userData: null,
        goHome: false,
    };
    it('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle REQUEST_STARTED', () => {
        expect(reducer(initialState, {type: REQUEST_STARTED})).toEqual({...initialState, loading: true, error: null});
    });

    it('should handle GET_LIST_SUCCESS', () => {
        expect(
            reducer(initialState, {
                type: GET_LIST_SUCCESS,
                listData: [
                    {_id: '1', firstName: 'test'},
                    {_id: '2', firstName: 'test2'},
                ],
            }),
        ).toEqual({
            ...initialState,
            listData: [
                {_id: '1', firstName: 'test'},
                {_id: '2', firstName: 'test2'},
            ],
        });
    });

    it('should handle GET_USER_SUCCESS', () => {
        expect(reducer(initialState, {type: GET_USER_SUCCESS, userData: {_id: '1', firstName: 'test'}})).toEqual({
            ...initialState,
            userData: {_id: '1', firstName: 'test'},
        });
    });

    it('should handle CREATE_USER_SUCCESS', () => {
        const changedState = {
            ...initialState,
            listData: [
                {_id: '1', firstName: 'test'},
                {_id: '2', firstName: 'test2'},
            ],
        };
        expect(
            reducer(changedState, {
                type: CREATE_USER_SUCCESS,
                listData: [
                    {_id: '1', firstName: 'test'},
                    {_id: '2', firstName: 'test2'},
                    {_id: '3', firstName: 'test3'},
                ],
            }),
        ).toEqual({
            ...initialState,
            goHome: true,
            listData: [
                {_id: '1', firstName: 'test'},
                {_id: '2', firstName: 'test2'},
                {_id: '3', firstName: 'test3'},
            ],
        });
    });

    it('should handle UPDATE_USER_SUCCESS', () => {
        const changedState = {
            ...initialState,
            listData: [
                {_id: '1', firstName: 'test'},
                {_id: '2', firstName: 'test2'},
            ],
        };
        expect(
            reducer(changedState, {
                type: UPDATE_USER_SUCCESS,
                listData: [
                    {_id: '1', firstName: 'test'},
                    {_id: '2', firstName: 'test3'},
                ],
            }),
        ).toEqual({
            ...initialState,
            goHome: true,
            listData: [
                {_id: '1', firstName: 'test'},
                {_id: '2', firstName: 'test3'},
            ],
        });
    });

    it('should handle DELETE_USER_SUCCES', () => {
        const changedState = {
            ...initialState,
            listData: [
                {_id: '1', firstName: 'test'},
                {_id: '2', firstName: 'test2'},
                {_id: '3', firstName: 'test3'},
            ],
        };
        expect(
            reducer(changedState, {
                type: DELETE_USER_SUCCESS,
                goHome: true,
                listData: [
                    {_id: '1', firstName: 'test'},
                    {_id: '2', firstName: 'test2'},
                ],
            }),
        ).toEqual({
            ...initialState,
            goHome: true,
            listData: [
                {_id: '1', firstName: 'test'},
                {_id: '2', firstName: 'test2'},
            ],
        });
    });

    it('should handle RESET', () => {
        const changedState = {
            ...initialState,
            userData: {_id: '1', firstName: 'test'},
            error: '500 Internal',
            listData: [
                {_id: '1', firstName: 'test'},
                {_id: '2', firstName: 'test2'},
            ],
        };
        expect(
            reducer(changedState, {
                type: RESET,
            }),
        ).toEqual({
            ...initialState,
            userData: null,
            error: null,
            listData: null,
        });
    });

    it('should handle RESET_USER', () => {
        const changedState = {
            ...initialState,
            userData: {_id: '1', firstName: 'test'},
            goHome: true,
        };
        expect(
            reducer(changedState, {
                type: RESET_USER,
            }),
        ).toEqual({
            ...initialState,
            userData: null,
            goHome: false,
        });
    });

    it('should handle REQUEST_FAILED', () => {
        expect(
            reducer(initialState, {
                type: REQUEST_FAILED,
                error: '500 Internal error',
            }),
        ).toEqual({
            ...initialState,
            loading: false,
            error: '500 Internal error',
        });
    });
});
