/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import Modal from '../components/Modal';

const handleClose = jest.fn();
const handleConfirm = jest.fn();

describe('testing Modal component', () => {
    it('should render correctly if show is enabled', () => {
        const props = {
            show: true,
            onClose: handleClose,
            onConfirm: handleConfirm,
            title: 'Test Title',
            body: 'Test Content',
        };
        const {getByText} = render(<Modal {...props} />);
        expect(getByText('Test Title')).toBeTruthy();
        expect(getByText('Test Content')).toBeTruthy();
    });

    it('should render correctly without body', () => {
        const props = {
            show: true,
            title: 'Test Title',
            onClose: handleClose,
            onConfirm: handleConfirm,
        };
        const {getByText} = render(<Modal {...props} />);
        expect(getByText('Test Title')).toBeTruthy();
    });

    test('should close on handleClose', () => {
        const {getByText} = render(<Modal show onClose={handleClose} onConfirm={handleConfirm} title="modal title" />);

        expect(getByText('modal title')).toBeTruthy();

        fireEvent.click(getByText(/Close/i));

        expect(handleClose).toHaveBeenCalledTimes(1);
    });

    test('should confirm on handleConfirm', () => {
        const {getByText} = render(<Modal show onClose={handleClose} onConfirm={handleConfirm} title="modal title" />);

        expect(getByText('modal title')).toBeTruthy();

        fireEvent.click(getByText(/Delete User/i));

        expect(handleConfirm).toHaveBeenCalledTimes(1);
    });
});
