import React from 'react';
import {render, waitForElement} from '@testing-library/react';
import {BrowserRouter, MemoryRouter} from 'react-router-dom';
import {loadingSelector, errorSelector, userDataSelector, goHomeSelector} from '../store/selectors';
import UserPage from '../pages/UserPage';
import * as actions from '../store/actions';

jest.spyOn(actions, 'getUser');

afterEach(() => {
    loadingSelector.mockRestore();
    goHomeSelector.mockRestore();
    errorSelector.mockRestore();
    userDataSelector.mockRestore();
    jest.clearAllMocks();
});

describe('testing User Page', () => {
    it('should render correctly', () => {
        const {getByTestId} = render(
            <BrowserRouter>
                <UserPage />
            </BrowserRouter>,
        );
        expect(getByTestId('user-container')).toBeInTheDocument();
    });

    it('should dispatch getUser if on edit', () => {
        const match = {params: {id: '1234'}};
        render(
            <MemoryRouter initialEntries={['/users/1234']}>
                <UserPage match={match} />
            </MemoryRouter>,
        );
        expect(actions.getUser).toHaveBeenCalledTimes(1);
    });

    it('should NOT call dispatch if on create', () => {
        render(
            <MemoryRouter initialEntries={['/users/create']}>
                <UserPage />
            </MemoryRouter>,
        );
        expect(actions.getUser).toHaveBeenCalledTimes(0);
    });

    it('should show loading while loading', () => {
        loadingSelector.mockReturnValue(true);
        const {getByTestId} = render(
            <BrowserRouter>
                <UserPage />
            </BrowserRouter>,
        );

        const loadingBar = getByTestId('user-loading-bar');
        expect(loadingBar).toBeInTheDocument();
    });

    it('should redirect to error page if error found', () => {
        errorSelector.mockReturnValue(true);
        const {queryByTestId} = render(
            <BrowserRouter>
                <UserPage />
            </BrowserRouter>,
        );

        expect(window.location.pathname).toBe('/error');
        expect(queryByTestId('user-container')).not.toBeInTheDocument();
    });

    it('should redirect Home when goHome is true', () => {
        goHomeSelector.mockReturnValue(true);
        const {queryByTestId} = render(
            <BrowserRouter>
                <UserPage />
            </BrowserRouter>,
        );

        waitForElement(() => expect(window.location.pathname).toBe('/users'));
        expect(queryByTestId('user-container')).not.toBeInTheDocument();
    });

    it('should render user form when there are data', () => {
        userDataSelector.mockReturnValue({
            id: '1234',
            firstName: 'testName',
            lastName: 'testLast',
            email: 'test@last.com',
        });
        loadingSelector.mockReturnValue(false);
        const match = {params: {id: '1234'}};
        const {queryByTestId} = render(
            <BrowserRouter>
                <UserPage match={match} />
            </BrowserRouter>,
        );

        expect(queryByTestId('user-loading-bar')).not.toBeInTheDocument();
        waitForElement(() => expect(window.location.pathname).toBe('/users/1234'));
    });
});
