/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {render, waitForElement, fireEvent} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';
import {listDataSelector} from '../store/selectors';
import FIELDS from '../fields';
import TableList from '../components/TableList';
import * as actions from '../store/actions';

jest.spyOn(actions, 'deleteUser');

afterEach(() => {
    listDataSelector.mockRestore();
    jest.clearAllMocks();
});

describe('testing Table List', () => {
    it('should render correctly', () => {
        const props = {
            fields: FIELDS,
        };
        const {getByTestId} = render(<TableList {...props} />);
        expect(getByTestId('users-list')).toBeInTheDocument();
    });

    it('should render correctly the headers', () => {
        const props = {
            fields: FIELDS,
        };
        const {getByText} = render(<TableList {...props} />);
        expect(getByText('First Name')).toBeInTheDocument();
    });

    it('should show error message when users list is empty', () => {
        listDataSelector.mockReturnValue([]);
        const props = {
            fields: FIELDS,
        };
        const {getByTestId} = render(<TableList {...props} />);
        expect(getByTestId('no-users-text')).toBeInTheDocument();
        expect(getByTestId('table-body-data').children).toHaveLength(1);
    });

    it('should render correctly the actions and rows', () => {
        listDataSelector.mockReturnValue([
            {_id: 1, firstName: 'testName1', lastName: 'testLast1', email: 'test1@last.com'},
            {_id: 2, firstName: 'testName2', lastName: 'testLast2', email: 'test2@last.com'},
        ]);
        const props = {
            fields: FIELDS,
        };
        const {getByTestId, getAllByTestId} = render(
            <BrowserRouter>
                <TableList {...props} />
            </BrowserRouter>,
        );
        expect(getAllByTestId('edit-button')).toHaveLength(2);
        expect(getAllByTestId('delete-button')).toHaveLength(2);
        expect(getByTestId('table-body-data').children).toHaveLength(2);
    });

    it('should have correct edit button href link', () => {
        listDataSelector.mockReturnValue([
            {_id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'},
        ]);

        const props = {
            fields: FIELDS,
        };

        const {getByTestId} = render(
            <BrowserRouter>
                <TableList {...props} />
            </BrowserRouter>,
        );

        const editBtn = getByTestId('edit-button');
        expect(editBtn.closest('a')).toHaveAttribute('href', `/users/${1234}`);
    });

    it('should click on delete button, open dialog and delete', () => {
        listDataSelector.mockReturnValue([
            {_id: '1234', firstName: 'testName', lastName: 'testLast', email: 'test@last.com'},
        ]);

        const props = {
            fields: FIELDS,
        };

        const {getByTestId, getByText} = render(
            <BrowserRouter>
                <TableList {...props} />
            </BrowserRouter>,
        );

        const deleteBtn = getByTestId('delete-button');
        fireEvent.click(deleteBtn);
        waitForElement(() => expect(getByText('DELETE USER')).toBeInTheDocument());
        const confirmDeleteBtn = getByTestId('confirm-delete-button');
        fireEvent.click(confirmDeleteBtn);
        expect(actions.deleteUser).toHaveBeenCalledTimes(1);
        waitForElement(() => expect(getByTestId('table-body-data').children).toHaveLength(0));
    });
});
