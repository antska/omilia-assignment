import React from 'react';
import ReactDOM from 'react-dom';
import {render, fireEvent} from '@testing-library/react';
import {BrowserRouter, MemoryRouter} from 'react-router-dom';
import NavBar from '../components/Navbar';

let container;
beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
});
afterEach(() => {
    document.body.removeChild(container);
    container = null;
});

describe('testing Navbar component', () => {
    it('should render correclty', () => {
        ReactDOM.render(
            <BrowserRouter>
                <NavBar />
            </BrowserRouter>,
            container,
        );
        expect(container.textContent).toBe('Team');
    });

    it('clicks on home button redirect to home', () => {
        const {getByTestId} = render(
            <MemoryRouter initialEntries={['/users/create']}>
                <NavBar />
            </MemoryRouter>,
        );

        const homeButton = getByTestId('home-button');
        fireEvent.click(homeButton);
        expect(window.location.pathname).toBe('/');
    });

    it('should show Create button on home and have correct href', () => {
        const {getByTestId} = render(
            <MemoryRouter initialEntries={['/users']}>
                <NavBar location={{pathname: '/users'}} />
            </MemoryRouter>,
        );

        const createBtn = getByTestId('create-button');
        expect(createBtn).toBeInTheDocument();
        expect(createBtn.closest('a')).toHaveAttribute('href', `/users/create`);
        fireEvent.click(createBtn);
        expect(window.location.pathname).toBe('/');
    });

    it('should NOT show Create button on other page', () => {
        const {queryByTestId} = render(
            <MemoryRouter initialEntries={['/users/create']}>
                <NavBar location={{pathname: '/users/create'}} />
            </MemoryRouter>,
        );

        expect(queryByTestId('create-button')).not.toBeInTheDocument();
    });
});
