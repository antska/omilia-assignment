import React from 'react';
import PropTypes from 'prop-types';
import {faTrash} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {ActionButton} from '../styles/shared';
import {ModalWrapper, ModalContent, ModalTitle, ModalActions} from './styles/Modal.styled';

/* Modal Component
   It is used to show a modal in screen with confirm and close buttons
*/
const Modal = ({show, onClose, onConfirm, title, body}) => {
    if (!show) {
        return null;
    }

    return (
        <ModalWrapper>
            <ModalContent>
                <ModalTitle>{title}</ModalTitle>
                <div>{body}</div>
                <ModalActions>
                    <ActionButton type="button" onClick={onClose}>
                        Close
                    </ActionButton>
                    <ActionButton delete type="button" onClick={onConfirm} data-testid="confirm-delete-button">
                        <FontAwesomeIcon color="#ca4c58" size="1x" icon={faTrash} />
                        Delete User
                    </ActionButton>
                </ModalActions>
            </ModalContent>
        </ModalWrapper>
    );
};

Modal.propTypes = {
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    show: PropTypes.bool,
    body: PropTypes.string,
};

Modal.defaultProps = {
    show: null,
    body: '',
};

export default Modal;
