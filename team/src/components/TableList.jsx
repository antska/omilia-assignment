/* eslint-disable no-underscore-dangle */
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {deleteUser} from '../store/actions';
import {listDataSelector} from '../store/selectors';
import Modal from './Modal';
import {
    TableWrapper,
    FullWidthTable,
    TableCellHead,
    TableCellCenter,
    FabButtonDel,
    TableCell,
    FabButton,
} from './styles/TableList.styled';

/* TableList Component
   It is used to render a full width table with all the users
   The columns come from a generic file that contains
   API's fields.
*/
const TableList = ({fields}) => {
    const [openDialog, setOpenDialog] = useState(false);
    const [selectedUser, setSelectedUser] = useState(null);
    const listData = useSelector(listDataSelector);
    const dispatch = useDispatch();

    const onDelete = userId => {
        setOpenDialog(true);
        setSelectedUser(userId);
    };

    const handleClose = () => {
        setOpenDialog(false);
    };

    const confirmDeleteUser = () => {
        dispatch(deleteUser(selectedUser, false));
        handleClose();
    };

    return (
        <TableWrapper>
            <FullWidthTable data-testid="users-list">
                <thead>
                    <tr>
                        <TableCellHead>#</TableCellHead>
                        {Object.keys(fields).map(field => (
                            <TableCellHead padding="default" key={field}>
                                {fields[field].label}
                            </TableCellHead>
                        ))}
                        <TableCellHead>Actions</TableCellHead>
                    </tr>
                </thead>
                <tbody data-testid="table-body-data">
                    {listData && listData.length > 0 ? (
                        listData.map((item, index) => (
                            <tr key={`row_${item._id}`}>
                                <TableCell>{index + 1}</TableCell>
                                {Object.keys(fields).map(field => (
                                    <TableCell padding="default" key={field}>
                                        {item[field]}
                                    </TableCell>
                                ))}
                                <TableCell>
                                    <Link to={`/users/${item._id}`}>
                                        <FabButton>
                                            <FontAwesomeIcon
                                                aria-label="edit button"
                                                color="white"
                                                size="sm"
                                                icon={faEdit}
                                                data-testid="edit-button"
                                            />
                                        </FabButton>
                                    </Link>
                                    <FabButtonDel onClick={e => onDelete(item._id)}>
                                        <FontAwesomeIcon
                                            aria-label="delete button"
                                            color="white"
                                            size="sm"
                                            icon={faTrash}
                                            data-testid="delete-button"
                                        />
                                    </FabButtonDel>
                                </TableCell>
                            </tr>
                        ))
                    ) : (
                        <tr>
                            <TableCellCenter colSpan={5}>
                                <h2 data-testid="no-users-text">No users found!</h2>
                            </TableCellCenter>
                        </tr>
                    )}
                </tbody>
            </FullWidthTable>
            {openDialog && (
                <Modal
                    show={openDialog}
                    onClose={handleClose}
                    onConfirm={confirmDeleteUser}
                    title="Are you sure?"
                    body="By deleting this user he will be removed from the list."
                />
            )}
        </TableWrapper>
    );
};

TableList.propTypes = {
    fields: PropTypes.shape({
        firstName: PropTypes.shape({label: PropTypes.string}),
        lastName: PropTypes.shape({label: PropTypes.string}),
        email: PropTypes.shape({label: PropTypes.string}),
    }).isRequired,
};

export default TableList;
