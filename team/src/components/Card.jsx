import React from 'react';
import PropTypes from 'prop-types';
import {Nav, Menu} from '../styles/shared';
import {CardContainer, CardTitle} from './styles/Card.styled';

/* Card Component
   It is used to display a Card element with menu bar or without
*/
const Card = ({title, children}) => {
    return (
        <CardContainer data-testid="card-container">
            {title && (
                <Nav>
                    <Menu>
                        <CardTitle data-testid="card-menu-title">{title}</CardTitle>
                    </Menu>
                </Nav>
            )}
            <div>{children}</div>
        </CardContainer>
    );
};

Card.propTypes = {
    title: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

Card.defaultProps = {
    title: null,
};

export default Card;
