import styled from 'styled-components';

const ModalWrapper = styled.div`
    z-index: 3;
    display: block;
    padding-top: 200px;
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0, 0, 0);
    background-color: rgba(0, 0, 0, 0.4);
`;

const ModalContent = styled.div`
    margin: auto;
    background-color: #fff;
    position: relative;
    outline: 0;
    width: 380px;
    box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.2), 0 4px 20px 0 rgba(0, 0, 0, 0.19);
    padding: 24px;
    border-radius: 7px;
`;

const ModalTitle = styled.h2`
    margin: 0 0 30px 0;
    font-weight: 400;
`;

const ModalActions = styled.div`
    padding: 0.5rem 0rem;
    margin-top: 20px;
    display: flex;
    justify-content: flex-end;
`;

export {ModalWrapper, ModalContent, ModalTitle, ModalActions};
