import styled from 'styled-components';

const TableWrapper = styled.div`
    width: 100%;
    overflow-x: auto;
    box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14),
        0px 1px 5px 0px rgba(0, 0, 0, 0.12);
    border: solid 1px rgba(0, 0, 0, 0.27);
    border-radius: 3px;
`;

const FullWidthTable = styled.table`
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
`;

const TableCell = styled.td`
    padding: 16px;
    font-size: 0.875rem;
    text-align: left;
    border-bottom: 1px solid rgba(224, 224, 224, 1);
`;

const TableCellHead = styled(TableCell)`
    font-size: 1.1rem;
`;

const TableCellCenter = styled(TableCell)`
    text-align: center;
    & > h2 {
        font-weight: 400;
    }
`;

const FabButton = styled.div`
    width: 40px;
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 50%;
    background-color: #6b7a8e;
    text-decoration: none;
    color: inherit;
    display: inline-flex;
    cursor: pointer;
    box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14),
        0px 1px 18px 0px rgba(0, 0, 0, 0.12);

    &:hover {
        background-color: #8c9cb1;
    }
`;

const FabButtonDel = styled(FabButton)`
    background-color: #ca4c58;
    &:hover {
        background-color: #a95960;
    }
`;

export {TableWrapper, FullWidthTable, TableCell, TableCellHead, TableCellCenter, FabButton, FabButtonDel};
