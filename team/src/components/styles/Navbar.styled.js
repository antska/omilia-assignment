import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const MenuLink = styled(Link)`
    padding: 12px;
    font-size: 1.5rem;
    text-align: center;
`;

const AddButton = styled.button`
    background-color: rgb(214, 176, 119);
    border-radius: 8px;
    border: none;
    color: white;
    padding: 11px 24px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14),
        0px 1px 18px 0px rgba(0, 0, 0, 0.12);
    cursor: pointer;
    transition-duration: 0.4s;

    &:hover {
        background-color: rgb(148, 125, 91);
        color: white;
    }

    &:active,
    &:focus {
        outline: none;
    }
`;

const HomeIcon = styled(FontAwesomeIcon)`
    &:hover {
        color: #dcc7aa;
    }
`;

const AddIcon = styled(FontAwesomeIcon)`
    margin-left: 10px;
`;

export {MenuLink, AddButton, HomeIcon, AddIcon};
