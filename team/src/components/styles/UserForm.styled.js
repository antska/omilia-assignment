import styled from 'styled-components';
import {ActionButton} from '../../styles/shared';

const Grid = styled.div`
    display: grid;
    grid-template-rows: repeat(4, 60px);
    grid-row-gap: 8px;
    justify-items: stretch;
    padding: 10px;
`;

const GridItem = styled.div`
    align-self: self-end;
    border-bottom: 1px solid #ccc;
`;

const Input = styled.input`
    width: 100%;
    border: 0;
    padding: 8px 0px;
    font-size: 1rem;
    margin-left: 10px;

    &:focus {
        outline: 0;
        background: white;
    }
`;

const HelperText = styled.div`
    color: #ccc;
    font-size: 0.8em;
`;

const ErrorText = styled.div`
    color: #c73030;
    max-height: 0;
    font-style: italic;
    font-size: 0.8em;
`;

const InputWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-content: center;
    align-items: center;
`;

const Form = styled.form`
    border: 2px solid rgba(0, 0, 0, 0.12);
`;

const FormActions = styled.div`
    display: flex;
    align-self: self-end;
    justify-content: space-between;
`;

const SaveButton = styled(ActionButton)`
    opacity: ${props => props.disabled && 0.6};
    cursor: ${props => props.disabled && 'not-allowed'};
    color: black;
    justifty-self: self-end;
    &:hover {
        color: ${props => (props.disabled ? 'black' : 'white')};
        background-color: ${props => (props.disabled ? 'none' : '#91d896')};
        border-radius: ${props => (props.disabled ? 'none' : '7px')};
        box-shadow: ${props => (props.disabled ? 'none' : '')};
    }
`;

const DeleteButton = styled(ActionButton)`
    color: black;
    background-color: #ca4c58;
    &:hover {
        background-color: #ca4c58;
        color: white;
        border-radius: 7px;
    }
`;
export {Grid, GridItem, Input, HelperText, ErrorText, InputWrapper, SaveButton, DeleteButton, Form, FormActions};
