import styled from 'styled-components';
import device from '../../styles/device';

const CardContainer = styled.div`
    width: 400px;
    margin: 10px;
    @media ${device.less.mobileL} {
        width: 300px;
    }

    @media ${device.less.mobileM} {
        width: 250px;
    }
`;

const CardTitle = styled.h4`
    color: white;
    font-weight: 500;
    letter-spacing: 1px;
`;

export {CardContainer, CardTitle};
