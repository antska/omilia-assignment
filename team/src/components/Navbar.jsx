import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import {useDispatch} from 'react-redux';
import {faHome, faPlus} from '@fortawesome/free-solid-svg-icons';

import {RESET} from '../store/constants';
import {Nav, Menu, TitleWrapper, Title} from '../styles/shared';
import {MenuLink, HomeIcon, AddButton, AddIcon} from './styles/Navbar.styled';

/* NavBar Component
   Navigation static bar used for both application header
   and in card component as well
*/
const NavBar = ({location}) => {
    const showAddBtn = location && location.pathname === '/users';
    const dispatch = useDispatch();
    return (
        <Nav data-testid="navbar">
            <Menu aria-label="Menu">
                <TitleWrapper>
                    <MenuLink to="/users" onClick={() => dispatch({type: RESET})}>
                        <HomeIcon aria-label="Home" color="white" size="lg" icon={faHome} data-testid="home-button" />
                    </MenuLink>
                    <Title>Team</Title>
                </TitleWrapper>
                {showAddBtn && (
                    <Link to="/users/create">
                        <AddButton type="button">
                            Add new user
                            <AddIcon
                                aria-label="Home"
                                color="white"
                                size="sm"
                                icon={faPlus}
                                data-testid="create-button"
                            />
                        </AddButton>
                    </Link>
                )}
            </Menu>
        </Nav>
    );
};

NavBar.propTypes = {
    location: PropTypes.shape({
        pathname: PropTypes.string,
    }),
};

NavBar.defaultProps = {
    location: {},
};

export default NavBar;
