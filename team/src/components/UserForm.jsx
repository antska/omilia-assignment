/* eslint-disable no-underscore-dangle */
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useDispatch} from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {createUser, updateUser, deleteUser} from '../store/actions';
import {
    Grid,
    GridItem,
    InputWrapper,
    Input,
    HelperText,
    SaveButton,
    DeleteButton,
    FormActions,
    Form,
    ErrorText,
} from './styles/UserForm.styled';
import Card from './Card';

/* UserForm Component
   It is used to render the form of the user (edit or create)
   It is wrapped in a Card component and uses validation rules
   on field change.
*/
const UserForm = ({user, fields, action}) => {
    const [values, setValues] = useState({
        firstName: (user && user.firstName) || '',
        lastName: (user && user.lastName) || '',
        email: (user && user.email) || '',
    });
    const [errors, setErrors] = useState({firstName: undefined, lastName: undefined, email: undefined});
    const dispatch = useDispatch();

    const onHandleChange = (event, fieldName) => {
        setValues({...values, [fieldName]: event.target.value});
        onHandleValidation(event.target.value, fieldName);
    };

    const onHandleValidation = (fieldValue, fieldName) => {
        if (!fieldValue) {
            setErrors({...errors, [fieldName]: 'Required'});
        } else if (fieldName === 'email' && fieldValue) {
            if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(fieldValue)) {
                setErrors({...errors, [fieldName]: 'Invalid email address'});
            } else {
                setErrors({...errors, [fieldName]: undefined});
            }
        } else {
            setErrors({...errors, [fieldName]: undefined});
        }
    };

    const onSubmit = event => {
        event.preventDefault();
        if (action === 'create') {
            dispatch(createUser(values));
        } else if (action === 'edit') {
            dispatch(updateUser(user._id, values));
        }
    };

    const onDelete = (event, userId) => {
        event.preventDefault();
        dispatch(deleteUser(userId, true));
    };

    return (
        <Card title={action === 'edit' ? 'EDIT USER' : 'CREATE USER'} data-testid="user-form-container">
            <Form autoComplete="off">
                <Grid>
                    {fields &&
                        Object.keys(fields).map(field => (
                            <GridItem key={`grid-item-${field}`}>
                                <HelperText>{fields[field].label}</HelperText>
                                <InputWrapper>
                                    <FontAwesomeIcon color="grey" size="sm" icon={fields[field].icon} />
                                    <Input
                                        type={fields[field].type}
                                        name={`field-${field}`}
                                        id={`id-${field}`}
                                        placeholder={fields[field].label}
                                        value={values[field]}
                                        onChange={e => onHandleChange(e, field)}
                                        aria-describedby={`input text ${field}`}
                                        aria-label={`input-${field}`}
                                        required
                                    />
                                </InputWrapper>
                                <ErrorText>{errors[field] !== undefined ? errors[field] : null}</ErrorText>
                            </GridItem>
                        ))}
                    <FormActions>
                        <SaveButton
                            type="submit"
                            data-testid="save-button"
                            contained
                            onClick={onSubmit}
                            disabled={
                                Object.values(errors).some(err => err !== undefined) ||
                                Object.values(values).some(val => val === '')
                            }>
                            Save
                        </SaveButton>
                        {action === 'edit' && (
                            <DeleteButton
                                type="button"
                                contained
                                onClick={e => onDelete(e, user._id)}
                                data-testid="user-delete-button">
                                Delete User
                            </DeleteButton>
                        )}
                    </FormActions>
                </Grid>
            </Form>
        </Card>
    );
};

UserForm.propTypes = {
    user: PropTypes.shape({
        _id: PropTypes.string,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        email: PropTypes.string,
    }),
    fields: PropTypes.shape({
        firstName: PropTypes.shape({label: PropTypes.string, type: PropTypes.string}),
        lastName: PropTypes.shape({label: PropTypes.string, type: PropTypes.string}),
        email: PropTypes.shape({label: PropTypes.string, type: PropTypes.string}),
    }).isRequired,
    action: PropTypes.string.isRequired,
};

UserForm.defaultProps = {
    user: null,
};

export default UserForm;
