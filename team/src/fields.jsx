import {faUser, faAt} from '@fortawesome/free-solid-svg-icons';

const fields = {
    firstName: {
        label: 'First Name',
        type: 'text',
        icon: faUser,
    },
    lastName: {
        label: 'Last Name',
        type: 'text',
        icon: faUser,
    },
    email: {
        label: 'Email',
        type: 'email',
        icon: faAt,
    },
};

export default fields;
