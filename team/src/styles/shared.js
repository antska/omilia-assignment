import styled from 'styled-components';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const Container = styled.div`
    height: 100%;
    margin: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;
const Nav = styled.nav`
    position: static;
    box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14),
        0px 1px 10px 0px rgba(0, 0, 0, 0.12);
    z-index: 1000;
    width: 100%;
    background-color: #6b7a8f;
`;

const Menu = styled.div`
    display: flex;
    min-height: 65px;
    align-items: center;
    justify-content: space-between;
    padding-left: 24px;
    padding-right: 24px;
`;

const Title = styled.h6`
    display: block;
    font-size: 1.25rem;
    font-weight: 500;
    margin: 0;
    color: white;
`;

const TitleWrapper = styled.div`
    display: flex;
    align-items: center;
`;

const ActionButton = styled.button`
    border: 0;
    padding: 17px 17px;
    font-size: 0.8rem;
    line-height: 1;
    text-decoration: none;
    cursor: pointer;
    white-space: no-wrap;
    background-color: ${props => (props.contained ? '#d8d8d8' : 'transparent')};
    color: ${props => (props.delete ? '#ca4c58' : '#8c9cb1')};
    font-weight: 600;
    text-transform: uppercase;
    border-radius: ${props => props.contained && '7px'};
    letter-spacing: 1px;
    transition: 0.9s;

    & > svg {
        margin-right: 5px;
    }

    &:hover {
        background-color: ${props => (props.contained ? '#d4d4d4' : 'rgba(234, 234, 234, 0.25)')};
        border-radius: 7px;
        box-shadow: ${props =>
            props.contained
                ? '0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0), 0px 1px 18px 0px rgba(0, 0, 0, 0.16)'
                : ''};
    }

    &:active,
    &:focus {
        outline: none;
    }
`;

const LoadingFontAwesomeIcon = styled(FontAwesomeIcon)`
    margin: 0 auto;
    position: absolute;
    top: 50%;
    display: inline-block;
    width: 40px;
    height: 40px;
`;

const CenterTypo = styled.h2`
    text-align: center;
    font-weight: 500;
`;

const CenterPar = styled.p`
    text-align: center;
`;

export {Container, Nav, Menu, Title, TitleWrapper, ActionButton, LoadingFontAwesomeIcon, CenterTypo, CenterPar};
