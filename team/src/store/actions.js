import {
    REQUEST_FAILED,
    REQUEST_STARTED,
    GET_LIST_SUCCESS,
    GET_USER_SUCCESS,
    API_ENDPOINT,
    DELETE_USER_SUCCESS,
    CREATE_USER_SUCCESS,
    UPDATE_USER_SUCCESS,
} from './constants';

/* getUsersList
   Action to get all users from the API
   dispatches REQUEST_STARTED
   GET_LIST_SUCCESS with results
   or REQUEST_FAILED on error
*/
export const getUsersList = () => async dispatch => {
    const abortController = new AbortController();

    dispatch({type: REQUEST_STARTED});
    let response;
    setTimeout(() => abortController.abort(), 5000);

    try {
        response = await fetch(`${API_ENDPOINT}/users`, {
            method: 'GET',
            signal: abortController.signal,
        });

        if (!response.ok) {
            const apiError = await response.json();
            throw new Error(`${response.status} ${response.statusText} ${(apiError && apiError.message) || ''}`);
        }
        const results = await response.json();
        dispatch({
            type: GET_LIST_SUCCESS,
            listData: results.data,
        });
    } catch (err) {
        if (!abortController.signal.aborted) {
            dispatch({
                type: REQUEST_FAILED,
                error: err.message,
            });
        } else if (err.name === 'AbortError') {
            dispatch({
                type: REQUEST_FAILED,
                error: 'Request took more than 5 seconds. Automatically cancelled.',
            });
        }
    }
};

/* getUser
   Action to get a specific user from the API
   dispatches REQUEST_STARTED
   GET_USER_SUCCESS with user data
   or REQUEST_FAILED on error
*/
export const getUser = id => async dispatch => {
    const abortController = new AbortController();

    dispatch({type: REQUEST_STARTED});
    let response;
    setTimeout(() => abortController.abort(), 5000);

    try {
        response = await fetch(`${API_ENDPOINT}/users/${id}`, {
            method: 'GET',
            signal: abortController.signal,
        });

        if (!response.ok) {
            const apiError = await response.json();
            throw new Error(`${response.status} ${response.statusText} ${(apiError && apiError.message) || ''}`);
        }
        const results = await response.json();
        dispatch({
            type: GET_USER_SUCCESS,
            userData: results.data,
        });
    } catch (err) {
        if (!abortController.signal.aborted) {
            dispatch({
                type: REQUEST_FAILED,
                error: err.message,
            });
        } else if (err.name === 'AbortError') {
            dispatch({
                type: REQUEST_FAILED,
                error: 'Request took more than 5 seconds. Automatically cancelled.',
            });
        }
    }
};

/* createUser
   Action to create a user
   dispatches REQUEST_STARTED
   CREATE_USER_SUCCESS with new user list data
   or REQUEST_FAILED on error
*/
export const createUser = data => async dispatch => {
    const abortController = new AbortController();

    dispatch({type: REQUEST_STARTED});
    let response;
    setTimeout(() => abortController.abort(), 5000);

    try {
        response = await fetch(`${API_ENDPOINT}/users`, {
            method: 'POST',
            signal: abortController.signal,
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(data),
        });

        if (!response.ok) {
            const apiError = await response.json();
            throw new Error(`${response.status} ${response.statusText} ${(apiError && apiError.message) || ''}`);
        }
        const results = await response.json();
        dispatch({
            type: CREATE_USER_SUCCESS,
            listData: results.data,
        });
    } catch (err) {
        if (!abortController.signal.aborted) {
            dispatch({
                type: REQUEST_FAILED,
                error: err.message,
            });
        } else if (err.name === 'AbortError') {
            dispatch({
                type: REQUEST_FAILED,
                error: 'Request took more than 5 seconds. Automatically cancelled.',
            });
        }
    }
};

/* updateUser
   Action to update a user
   dispatches REQUEST_STARTED
   UPDATE_USER_SUCCESS with new user list data
   or REQUEST_FAILED on error
*/
export const updateUser = (id, data) => async dispatch => {
    const abortController = new AbortController();

    dispatch({type: REQUEST_STARTED});
    let response;
    setTimeout(() => abortController.abort(), 5000);

    try {
        response = await fetch(`${API_ENDPOINT}/users/${id}`, {
            method: 'PUT',
            signal: abortController.signal,
            headers: new Headers({'Content-Type': 'application/json'}),
            body: JSON.stringify(data),
        });

        if (!response.ok) {
            const apiError = await response.json();
            throw new Error(`${response.status} ${response.statusText} ${(apiError && apiError.message) || ''}`);
        }
        const results = await response.json();
        dispatch({
            type: UPDATE_USER_SUCCESS,
            listData: results.data,
        });
    } catch (err) {
        if (!abortController.signal.aborted) {
            dispatch({
                type: REQUEST_FAILED,
                error: err.message,
            });
        } else if (err.name === 'AbortError') {
            dispatch({
                type: REQUEST_FAILED,
                error: 'Request took more than 5 seconds. Automatically cancelled.',
            });
        }
    }
};

/* deleteUser
   Action to delete a user
   dispatches REQUEST_STARTED
   DELETE_USER_SUCCESS with new user list data
   or REQUEST_FAILED on error
   
*/
export const deleteUser = (id, goHome) => async dispatch => {
    const abortController = new AbortController();

    dispatch({type: REQUEST_STARTED});
    let response;
    setTimeout(() => abortController.abort(), 5000);

    try {
        response = await fetch(`${API_ENDPOINT}/users/${id}`, {
            method: 'DELETE',
            signal: abortController.signal,
        });

        if (!response.ok) {
            const apiError = await response.json();
            throw new Error(`${response.status} ${response.statusText} ${(apiError && apiError.message) || ''}`);
        }
        const results = await response.json();
        dispatch({
            type: DELETE_USER_SUCCESS,
            goHome,
            listData: results.data,
        });
    } catch (err) {
        if (!abortController.signal.aborted) {
            dispatch({
                type: REQUEST_FAILED,
                error: err.message,
            });
        } else if (err.name === 'AbortError') {
            dispatch({
                type: REQUEST_FAILED,
                error: 'Request took more than 5 seconds. Automatically cancelled.',
            });
        }
    }
};
