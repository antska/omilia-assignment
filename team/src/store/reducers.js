import {
    REQUEST_STARTED,
    REQUEST_FAILED,
    DELETE_USER_SUCCESS,
    GET_LIST_SUCCESS,
    GET_USER_SUCCESS,
    CREATE_USER_SUCCESS,
    UPDATE_USER_SUCCESS,
    RESET,
    RESET_USER,
} from './constants';

const initialState = {
    loading: false,
    listData: null,
    error: null,
    userData: null,
    goHome: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case REQUEST_STARTED:
            return {
                ...state,
                loading: true,
                error: null,
                goHome: false,
            };
        case GET_LIST_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                goHome: false,
                listData: action.listData,
            };
        case GET_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                goHome: false,
                userData: action.userData,
            };
        case CREATE_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                userData: null,
                listData: action.listData,
                goHome: true,
            };
        case UPDATE_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                userData: null,
                listData: action.listData,
                goHome: true,
            };
        case DELETE_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                goHome: action.goHome,
                userData: null,
                listData: action.listData,
            };
        case REQUEST_FAILED:
            return {
                ...state,
                loading: false,
                goHome: false,
                error: action.error,
            };
        case RESET_USER:
            return {
                ...state,
                loading: false,
                error: null,
                userData: null,
                goHome: false,
            };
        case RESET:
            return {
                ...state,
                loading: false,
                error: null,
                goHome: false,
                userData: null,
                listData: null,
            };
        default:
            return state;
    }
};

export {initialState, reducer};
