export const listDataSelector = state => state.listData;
export const userDataSelector = state => state.userData;
export const loadingSelector = state => state.loading;
export const errorSelector = state => state.error;
export const goHomeSelector = state => state.goHome;
