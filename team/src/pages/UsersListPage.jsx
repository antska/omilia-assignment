import React, {useEffect} from 'react';
import {Redirect} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';
import {faSpinner} from '@fortawesome/free-solid-svg-icons';
import {LoadingFontAwesomeIcon, Container} from '../styles/shared';
import FIELDS from '../fields';
import TableList from '../components/TableList';
import {getUsersList} from '../store/actions';
import {loadingSelector, listDataSelector, errorSelector, goHomeSelector} from '../store/selectors';
import {RESET_USER} from '../store/constants';

/* Users List Page
   It is the page that contains all the Users details. 
   Parent of Table List component that renders the actual data of the users.
*/
const UsersListPage = () => {
    const loading = useSelector(loadingSelector);
    const listData = useSelector(listDataSelector);
    const error = useSelector(errorSelector);
    const goHome = useSelector(goHomeSelector);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!listData) dispatch(getUsersList());
        if (goHome) dispatch({type: RESET_USER});
    }, [goHome, listData, dispatch]);

    return error ? (
        <Redirect to="/error" />
    ) : (
        <Container data-testid="home-container">
            {loading && (
                <LoadingFontAwesomeIcon
                    data-testid="loading-bar"
                    aria-label="loading"
                    color="#DCC7AA"
                    size="5x"
                    icon={faSpinner}
                    spin
                />
            )}
            {listData && <TableList fields={FIELDS} />}
        </Container>
    );
};

export default UsersListPage;
