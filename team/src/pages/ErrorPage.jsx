import React from 'react';
import {Link} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faHome} from '@fortawesome/free-solid-svg-icons';
import {Actions, ErrorImage, BackHomeButton} from './styles/ErrorPage.styled';
import {Container, CenterTypo, CenterPar} from '../styles/shared';
import errorImage from '../img/error.gif';
import {errorSelector} from '../store/selectors';
import {RESET} from '../store/constants';
import Card from '../components/Card';

/* Error Page
   It is rendered whenever an error occurs and shows the error text in a Card component.
*/
const ErrorPage = () => {
    const error = useSelector(errorSelector);
    const dispatch = useDispatch();

    return (
        <Container>
            <Card>
                <div>
                    <div>
                        <ErrorImage src={errorImage} title="Error Found" alt="Error Found" />
                        <CenterTypo data-testid="error-message">Oops! Something went wrong!</CenterTypo>
                        <CenterPar>{`${error || ''}`}</CenterPar>
                    </div>
                    <Actions>
                        <Link to="/users" onClick={() => dispatch({type: RESET})} data-testid="error-home-button">
                            <BackHomeButton type="button" contained>
                                <FontAwesomeIcon aria-label="Go Home" color="black" size="1x" icon={faHome} />
                                Home
                            </BackHomeButton>
                        </Link>
                    </Actions>
                </div>
            </Card>
        </Container>
    );
};

export default ErrorPage;
