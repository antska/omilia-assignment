import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {faSpinner} from '@fortawesome/free-solid-svg-icons';
import {Container, LoadingFontAwesomeIcon} from '../styles/shared';
import UserForm from '../components/UserForm';
import FIELDS from '../fields';
import {getUser} from '../store/actions';
import {loadingSelector, userDataSelector, errorSelector, goHomeSelector} from '../store/selectors';

/* User Page
   It is the page that contains the User info. 
   Parent of User Form component that renders the actual data.
*/
const UserPage = ({match}) => {
    const loading = useSelector(loadingSelector);
    const userData = useSelector(userDataSelector);
    const error = useSelector(errorSelector);
    const goHome = useSelector(goHomeSelector);
    const dispatch = useDispatch();

    const userId = match.params && match.params.id;

    useEffect(() => {
        if (userId) dispatch(getUser(userId));
    }, [userId, dispatch]);

    if (error) {
        return <Redirect to="/error" />;
    }

    if (goHome) {
        return <Redirect to="/" />;
    }

    return (
        <Container data-testid="user-container">
            {loading && (
                <LoadingFontAwesomeIcon
                    data-testid="user-loading-bar"
                    aria-label="loading"
                    color="#DCC7AA"
                    size="5x"
                    icon={faSpinner}
                    spin
                />
            )}
            {(userData || !userId) && <UserForm user={userData} fields={FIELDS} action={userId ? 'edit' : 'create'} />}
        </Container>
    );
};

UserPage.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            id: PropTypes.string,
        }),
    }),
};

UserPage.defaultProps = {
    match: {},
};
export default UserPage;
