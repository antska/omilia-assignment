import styled from 'styled-components';
import {ActionButton} from '../../styles/shared';

const BackHomeButton = styled(ActionButton)`
    color: black;
    background-color: #e65637;
    &:hover {
        background-color: #f17c63;
        border-radius: 7px;
    }
`;

const ErrorImage = styled.img`
    display: block;
    height: 250px;
    width: 250px;
    margin: 0 auto;
`;

const Actions = styled.div`
    display: flex;
    justify-content: center;
`;

export {BackHomeButton, ErrorImage, Actions};
