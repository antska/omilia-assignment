# Omilia Assignment - Team App

---

The task was to create a SPA with create-react-app named Team.
Assignment for Front-End Engineer position at [Omilia](https://omilia.com/).

## Description

---

Team App has been developed in React framework and with custom styling, 
using [Styled Components](https://styled-components.com/).
It consumes and external API with users who are been presented in a table list.

The basic functionalities are the following:

-   Listing all users.
-   Creating a new user.
-   Editing an existing user.
-   Deleting a user from list page or from user's page.
-   Redirect to error page if something goes wrong.

## Project structure

---

    .
    ├── team
    |   |
    │   cypress                 
    |   ├── integration         # *.spec.js files
    |   ├── ...
    ├── src     
    |   |── __tests__           # unit tests
    |   |── components          # basic components used
    |   |    |── styles         # styles folder for components
    |   |    |── ...            # components files
    |   |    |── Modal.jsx      
    |   ├── img                 # images
    |   |── pages               # pages of the app
    |   |    |── styles         # styles folder for pages
    |   |    |── ...            # pages files
    |   |    |── UserPage.jsx      
    |   |── store               # redux store, reducers, actions,...
    |   |── styles              # global styles
    |   |── App.jsx             # main App
    |   |── fields.jsx          # fields used (API columns)
    |   |── index.jsx           # index.jsx init file
    |   |── ...
    │   ├── .eslintrc.json      # eslint conf
    |   ├── .prettierrc         # prettier conf
    |   ├── package.json        # package.json
    |   ├── ...
    |   |
    │   .gitlab-ci.yml          # GitLab CI configuration.
    │   README.md               # README file.
    └── ...

> Font Awesome icons packages are used for icons styling.


#### Technologies Used

In this project React v16.12 was used along with [Redux](https://redux.js.org/) 
implementing [Hooks](https://react-redux.js.org/next/api/hooks).

The basic idea is to use a store that handles a global state. It uses actions, reducers and a middleware(thunk) in order to utilize API fetch calls as actions. On top of that, selectors are being used as well in order to access state easily.

#### Styled Components - Responsive layout

Regarding the styles used, the methodology in this project is to have a shared 
styles file that will contain global styles but also to have a styles folder in 
components & pages that will host custom styles for each separate component.

In order to have responsive layout of the whole app, CSS Grid and Flex Layout has 
been used across the application, along with a helper file that has device specific 
widths for media queries.

### Testing with [Jest](https://jestjs.io/) | [React Testing Library](https://testing-library.com/) | [Cypress](https://www.cypress.io/) | Gitlab CI

Unit/Integration tests are located in `__tests__` folder for each component.

Cypress tests are located in `cypress/integration` folder.

Gitlab CI has been configured to run automatically a procedure with stages:

*  install
*  build
*  quality: linting, unit tests, e2e tests

### Installation

``cd team``

``npm install``

``npm start``

### Running the tests

``cd team``

``npm run lint`` or `eslint 'src/**/*.{js,jsx}'`

``npm run test:coverage``

``npm run e2e:local``

